import lktrack

imnames = ['viff.000.ppm', 'viff.001.ppm', 'viff.002.ppm', 'viff.003.ppm', 'viff.004.ppm', 'viff.005.ppm', 'viff.006.ppm', 'viff.007.ppm', 'viff.008.ppm']

# create tracker object
lkt = lktrack.LKTracker(imnames)

# detect in first frame, track in the remaining 
lkt.detect_points()
lkt.draw()
for i in range(len(imnames) - 1):
    lkt.track_points()
    lkt.draw()
